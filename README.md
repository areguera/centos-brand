> ⛔ Development of this project is now at: https://gitlab.com/CentOS/artwork/centos-brand

# The CentOS Brand

The CentOS Brand is the main graphical identification of the CentOS Project.
The CentOS Artwork SIG uses the CentOS Brand to connect all the visual
manifestations the CentOS Project is made of and, this way, it provides visual
recognition among similar projects available on the Internet.

## Composition

The CentOS Brand is composed of a graphical component known as the CentOS
Symbol and a typographical component known as the CentOS Type. When the CentOS
Symbol and the CentOS Type are combined, they create what we know as the CentOS
Logo. All the components that make the CentOS Brand can be used together or
separately, considering that, in hierarchy order, the CentOS Logo is rather
preferred than the CentOS Symbol alone, as well as the CentOS Symbol is rather
preferred than the CentOS Type alone.

### The CentOS Symbol

![The CentOS Symbol](HowTos/centos-symbol-x256.png)

### The CentOS Type

![The CentOS Type](HowTos/centos-type-x128.png)

| Montserrat-Bold |
| --------------- |
| [Google Fonts](https://fonts.google.com/specimen/Montserrat) |
| [Git Repository](https://github.com/JulietaUla/Montserrat) |
| Licensed under the [Open Font License](https://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL) |

### The CentOS Logo

![The CentOS Logo](HowTos/centos-logo.png)

#### Contrast Ratio

![The CentOS Logo Contrast Ratio](HowTos/centos-logo-contrast.png)

## Propagation

The CentOS Brand must be exactly the same every time it is printed out, no
matter what the impression medium is. A route to reproduce the CentOS Brand
this way must be used in order to avoid reproduction mistakes of any kind when
final images are marked with it. To prevent such mistakes, the CentOS Artwork
SIG maintains the CentOS Brand in SVG format and uses open source tools like
[Inkscape](https://inkscape.org/) and [ImageMagick](https://imagemagick.org/)
in [Makefile](https://www.gnu.org/software/make/manual/make.html) files to
render images in different formats, sizes, and colors without affecting the
standard proportions of CentOS brands.

## Improvements

The CentOS Brand is difficult to change for many reasons, including strong
community attachment to it and legal barriers. However we understand that to
stay visually relevant the CentOS Brand needs to be constantly under review to
correct visual inconsistencies and improve the mark reproduction through all
visual media the CentOS Project manifests its existence on.  To articulate such
antagonistic need, the CentOS Artwork SIG uses "design contests".

A design contest starts when someone opens a new issue in CentOS Artwork SIG
and requests to change the CentOS Brand in some way. Once the issue is opened,
it serves to evaluate how much community traction that specific change has.
Effective requests provide very specific and detailed information to visually
illustrate the needs that motivated to open the issue.

To change the CentOS Brand, the proposal in CentOS Artwork SIG issue tracker
needs to pass the CentOS Artwork SIG and CentOS Board approval. With these
barriers passed, the change proposal is sent to Red Hat Brand department (in
the hands of the CentOS Community Manager) for evaluation and approval as
well.  Normally, this confirmation occurs publicly, in the specific issue where
the change request was originated or in one opened for it on the CentOS Board
issue tracker. At this point of time, it is fine to update the SVG files used
to produce the images spread on all CentOS visual manifestations (e.g.,
distribution, websites, and promotion stuff).

The following is a list of design contests seen in the past.

*  [(2021-2022) - Improving CentOS Symbol Contrast, Size and Meaning](https://git.centos.org/centos/Artwork/issue/5)
*  [(2019-2021) - Request and discussion](https://git.centos.org/centos/Artwork/issue/1) | [Presentation](https://docs.google.com/presentation/d/e/2PACX-1vTd3sPkYcbNshQykKxsw21wn3uLqTJm1-kAWJYXZVwps8qk-2modHJm_1anDbNg13A1jfcyE4vYjC5v/pub?start=false&loop=false&delayms=3000)

## License

Copyright © 2022 Alain Reguera Delgado

The CentOS Brand is released under the terms of
[Creative Commons Attribution-ShareAlike 4.0 International Public License](https://creativecommons.org/licenses/by-sa/4.0/legalcode),
and usage limited by the [CentOS Trademark Guidelines](https://www.centos.org/legal/trademarks/).
